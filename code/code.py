# ~~~~~~~~~~~~~~~
# imports
# ~~~~~~~~~~~~~~~

import time
import board
import analogio
import digitalio

# ~~~~~~~~~~~~~~~
# variables
# ~~~~~~~~~~~~~~~

# if the value coming back from the resistor is less than this value,
# the light comes on...
onOffValue = 65100

# photoresistor - uses the A0 pin on the trinket (also labeled as ~1 on the board)
photoresistor = analogio.AnalogIn(board.A0)

# led - uses the D3 pin on the trinket
led = digitalio.DigitalInOut(board.D3)

# variable to hold 
prevVal = 0

# boolean to hold the value for if the LED is on (True) or off (False)
onFlag = False

# ~~~~~~~~~~~~~~~
# initialization
# ~~~~~~~~~~~~~~~

# set pin for LED to be an output pin
led.direction = digitalio.Direction.OUTPUT

# ~~~~~~~~~~~~~~~
# program loop
# ~~~~~~~~~~~~~~~

while True:
    # un-comment the print line to send the value of the resistor 
    # to the serial connection for debugging purposes...
    #print(photoresistor.value)

    # minor optimization - only do real work if the value of the photoresistor
    # changed since the last loop through...
    if prevVal != photoresistor.value:
        prevVal = photoresistor.value
        if prevVal < onOffValue:
            onFlag = True
        else:
            onFlag = False

        led.value = onFlag

    time.sleep(1)

